package com.poc.register.utils;

import io.restassured.http.ContentType;
import static io.restassured.RestAssured.*;
import static org.hamcrest.CoreMatchers.containsString;

import org.json.simple.JSONObject;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

class RegisterApiTestApplicationTests {
	
	@Test
	public void getErrorAPITest() {
		baseURI = "http://20.44.38.200/";
		given().
			get("/managers").
		then().
			statusCode(404);
		
	}

	@Test
	void getAPITest() {
		baseURI = "http://20.44.38.200/";
		given().
			get("/hi").
		then().
			content(containsString("hi amrit!!")); 
	}

	@DataProvider(name = "dataForPost")
	public Object[][] dataForPost() {
		

		String excelPath = "./data/TestData.xlsx";
		String sheetName = "Sheet1";
		ExcelUtils excel = new ExcelUtils(excelPath, sheetName);
		
		int r = excel.getRowCount()-2;
		int c = excel.getColCount();
		Object[][] data = new Object[r][c];
		
		System.out.println("row,col:"+r+" "+c);
		
		for(int i=1;i<=r;i++) {
			for(int j=0;j<c;j++) {
				data[i-1][j]=excel.getCellData(i,j);
			}
		}
		System.out.println(data);
		return data;

	}

	@Test(dataProvider = "dataForPost")
	public void postAPITest(String id, String name, String exp, String skill) {
		baseURI = "http://20.44.38.200/";
		JSONObject request = new JSONObject();
		request.put("id",id);
		request.put("name",name);
		request.put("exp",exp);
		request.put("skill",skill);

		given().
		contentType(ContentType.JSON).
		accept(ContentType.JSON).
		header("Content-Type", "application/json").
		body(request.toJSONString()).
		when().
		post("/employees").
		then().
		statusCode(201).
		log().all();
	}

}
